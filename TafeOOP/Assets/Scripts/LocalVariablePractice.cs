﻿using UnityEngine;
using System.Collections;

public class LocalVariablePractice : MonoBehaviour {

	public string playerName;
	public string sentance;
	public GameObject lastCollidedObject;

	// Use this for initialization
	void Start () {
		SetPlayerName();
	}
	
	// Update is called once per frame
	void Update () {
		CreateSentance();
		CreateDungeonMessage();
	}

	private void SetPlayerName(){
		string tempName;
		tempName = "player Two";
		playerName = "player One";
		playerName = tempName;
	}

	private void CreateSentance(){
		string sentance = "";

		if(Input.GetKeyDown(KeyCode.Alpha1)){
			sentance += "My name is: " + playerName;
		}

		if(Input.GetKeyDown(KeyCode.Alpha2)){
			sentance += "\n";
			sentance += "Oner day i died, the end.";
		}

		if(sentance.Length > 0){
			print (sentance);
		}
	}

	private void CreateDungeonMessage(){
		string dungeonMessage = "";
		if(Input.GetKeyDown(KeyCode.I)){
			string message;
			message = "You have entered a dungeon.\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.O)){
			string message;
			message = "You see a dragon.\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.P)){
			string message;
			message = "You are fucked.\n";
			dungeonMessage += message;
		}
		if(Input.GetKeyDown(KeyCode.U)){
			string message;
			message = "You are the champion.\n";
			dungeonMessage += message;
		}
		if(Input.GetKey(KeyCode.Return)){
			print(sentance);
			sentance = "";
		}
		if(dungeonMessage.Length > 0){
			sentance += dungeonMessage;
		}
	}

	void OnCollisionEnter(Collision other){
		lastCollidedObject = other.gameObject;
	}
}
