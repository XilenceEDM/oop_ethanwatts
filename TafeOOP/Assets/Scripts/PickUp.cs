﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {

	public int recoverAmount;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DestroyPickUp(){
		Destroy(this.gameObject);
	}

	void AddHealth(Player targetPlayer){
		targetPlayer.myStats.maxHealth += recoverAmount;
	}

	private void OnTriggerEnter(Collider other){
		Player tP = other.gameObject.GetComponent<Player>();
		if(tP != null){
			AddHealth(tP);
			DestroyPickUp();
		}
	}
}
