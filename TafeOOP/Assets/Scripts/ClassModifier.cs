﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour {

	public ClassPractice classPrac;

	// Use this for initialization
	void Start () {
		classPrac.PrintNumber();
		classPrac.PrintDecimalNumber();
		classPrac.PrintBool();
		classPrac.PrintWord();
		classPrac.PrintOtherWord();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
