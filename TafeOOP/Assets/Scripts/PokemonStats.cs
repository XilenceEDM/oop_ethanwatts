﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PokemonStatistics{
	string name;
	int health;
	int attackPower;
	int defence;
	int specialAttack;
	int specialDefence;
	int speed;
	int EXP;
	int Level;
//maybe a substruct for types????	
}

public class PokemonStats : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


}
