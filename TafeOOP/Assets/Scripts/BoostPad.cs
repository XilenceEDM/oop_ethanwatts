﻿using UnityEngine;
using System.Collections;

public class BoostPad : MonoBehaviour {

	public float boostSpeed;
	private GameObject objPlayer;

	// Use this for initialization
	void Start () {
		boostSpeed = 30f;
		objPlayer = GameObject.FindGameObjectWithTag("Player");
	}

	private void BoostPlayer(Vector3 dir, float speed){
		objPlayer.GetComponent<Rigidbody>().AddForce(dir * speed, ForceMode.Impulse);
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Player"){
			BoostPlayer(transform.forward , boostSpeed);
		}
	}
}
