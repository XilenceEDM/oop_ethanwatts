﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct StructScope {
	public int number;
	public string message;
	public bool isTrue;
}

public class ScopePractice : MonoBehaviour {

	public int classNumber = 9;
	public bool exists;
	public string messageInABottle;
	public bool checkIf;
	public string checkMessage;

	private string secretMessage = "Secret!";

	public StructScope structTest;

	// Use this for initialization
	void Start () {
		bool exists;
		exists = true;
		this.exists = exists;
		ScopeFunction();

		structTest.number = 1;
		structTest.message = "message";
		structTest.isTrue = true;
	}
	
	// Update is called once per frame
	void Update () {
		string messageInABottle;
		messageInABottle = "Hello World!";
		this.messageInABottle = messageInABottle;
	
	}

	public void ScopeFunction(){
		bool checkIf;
		checkIf = true;
		this.checkIf = checkIf;
		if(checkIf == true){
			string checkMessage;
			checkMessage = "Checked!";
			this.checkMessage = checkMessage;
		}
	}


	//Get and Set
	public string GetSecretMessage(){

		return secretMessage;
	}

	public void ChangeSecretMessage(string newMessage){
		if(newMessage.Length > 0 && newMessage.Length < 10){
			secretMessage = newMessage;
		}else{
			Debug.LogWarning("Message changing incorrectly");
		}
	}

	public void PrintSecretMessage(){
		print (secretMessage);
	}
}
