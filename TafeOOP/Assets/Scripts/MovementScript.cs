﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MovementScript : MonoBehaviour {

	public float moveSpeed;
	public float maxSpeed;
	public float jumpSpeed;
	public float maxJump;
	public bool canJump;
	public Rigidbody myRB;
	public Transform spawnPoint;
	public Text scoreText;
	public float score;



	// Use this for initialization
	void Start () {
		myRB = gameObject.GetComponent<Rigidbody>();
		jumpSpeed = 0;
	}
	
	// Update is called once per frame
	void Update () {
		ConstantMovement();
		Jump();
		moveSpeed += Time.deltaTime;
		if(moveSpeed > maxSpeed){
			moveSpeed = maxSpeed;
		}else if(moveSpeed < 0){
			moveSpeed = 0;
		}
		if(jumpSpeed > maxJump){
			jumpSpeed = maxJump;
		}
	}

	void ConstantMovement(){
		transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
		score += Time.deltaTime * 5;
		scoreText.text = "Score: " + (int)score;
	}

	void Jump(){
		if(Input.GetKey(KeyCode.Space)){
			jumpSpeed += Time.deltaTime * 8;
		}

		if(Input.GetKeyUp(KeyCode.Space) && canJump){
			myRB.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
			canJump = false;
			jumpSpeed = 0;
		}
	}

	void OnCollisionEnter(Collision other){
		if(other.gameObject.tag == "Obstacle"){
			moveSpeed -= 4;
			Destroy(other.gameObject);
		}
		if(other.gameObject.tag == "Ground"){
			canJump = true;
		}
		if(other.gameObject.tag == "Death"){
			Application.LoadLevel(Application.loadedLevel);
			moveSpeed = 1;
		}
	}
}
