﻿using UnityEngine;
using System.Collections;

public class ScopeModifier : MonoBehaviour {

	public ScopePractice otherClass;
	public string changingMessage;
	public string messageCopy;

	// Use this for initialization
	void Start () {
		otherClass.ScopeFunction();
		otherClass.exists = true;
		otherClass.classNumber = 10;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return)){
			otherClass.PrintSecretMessage();
		}
		if(Input.GetKeyDown(KeyCode.Backspace)){
			otherClass.ChangeSecretMessage(changingMessage);
		}
		if(Input.GetKeyDown(KeyCode.Backslash)){
			messageCopy = otherClass.GetSecretMessage();
		
		}
	}

}
