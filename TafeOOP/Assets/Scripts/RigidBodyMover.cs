﻿using UnityEngine;
using System.Collections;

[System.Serializable] 
public struct KeyBinds{ 
	public KeyCode keyForward;
	public KeyCode keyLeft;
	public KeyCode keyBack;
	public KeyCode keyRight;
	public KeyCode keyJump;
}

public class RigidBodyMover : MonoBehaviour {

	private Rigidbody myRigidBody;
	public float moveSpeed;
	public float jumpSpeed;
	public KeyBinds myKeyBinds; 
	
	
	// Use this for initialization
	void Start () {
		moveSpeed = 15;
		jumpSpeed = 10;
		myRigidBody = GetComponent<Rigidbody>();
		KeyBindings();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void FixedUpdate(){
		PlayerMovement();
	}

	private void KeyBindings(){
		myKeyBinds.keyForward = KeyCode.W;
		myKeyBinds.keyLeft = KeyCode.A;
		myKeyBinds.keyRight = KeyCode.D;
		myKeyBinds.keyBack = KeyCode.S;
		myKeyBinds.keyJump = KeyCode.Space;
	}

	private void PlayerMovement(){

		if(Input.GetKeyDown(myKeyBinds.keyJump)){
			myRigidBody.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
		}
		if(Input.GetKey(myKeyBinds.keyForward)){
			print("should be moving forward");
			myRigidBody.AddForce(Vector3.forward * moveSpeed);
		}
		if(Input.GetKey(myKeyBinds.keyLeft)){
			print("should be moving left");
			myRigidBody.AddForce(Vector3.left * moveSpeed);
		}
		if(Input.GetKey(myKeyBinds.keyBack)){
			print("should be moving back");
			myRigidBody.AddForce(Vector3.back * moveSpeed);
		}
		if(Input.GetKey(myKeyBinds.keyRight)){
			print("should be moving right");
			myRigidBody.AddForce(Vector3.right * moveSpeed);
		}
	}
}
