﻿using UnityEngine;
using System.Collections;

public class NeedArrays : MonoBehaviour {

	public string student1;
	public string student2;
	public string student3;
	public string student4;
	public string student5;
	public string student6;
	public string student7;
	public string student8;
	public string student9;
	public string student10;

	// Use this for initialization
	void Start () {
		student1 = "Gypsy One";
		student2 = "Gypsy Two";
		student3 = "Gypsy Three";
		student4 = "Gypsy Four";
		student5 = "Gypsy Five";
		student6 = "Gypsy Six";
		student7 = "Gypsy Seven";
		student8 = "Gypsy Eight";
		student9 = "Gypsy Nine";
		student10 = "Gypsy Ten";
		RenameStudents();


	}
	
	// Update is called once per frame
	void Update () {
	}

	private void RenameStudents(){
		student1 = "Dead";
		student2 = "Dead";
		student3 = "Dead";
		student4 = "Dead";
		student5 = "Adopted";
		student6 = "Adopted";
		student7 = "Adopted";
		student8 = "Dead";
		student9 = "Adopted";
		student10 = "Adopted";

	}
}
