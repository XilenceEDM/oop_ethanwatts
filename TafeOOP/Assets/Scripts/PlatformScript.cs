﻿using UnityEngine;
using System.Collections;

public class PlatformScript : MonoBehaviour {

	public GameObject platform;
	public MovementScript movingSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void SpawnPlatform(){ 
		Vector3 spawnPos = new Vector3(transform.position.x + (10 + movingSpeed.moveSpeed), transform.position.y + Random.Range(-2, 2), transform.position.z);
		Instantiate(platform, spawnPos, Quaternion.identity);
	}

	void OnTriggerEnter(Collider other){
		if(other.tag == "Player"){
			SpawnPlatform();
		}
	}
}
