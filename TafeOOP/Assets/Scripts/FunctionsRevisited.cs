﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class FunctionsRevisited : MonoBehaviour {

	public string myName;
	public int myAge;
	public string myGender;
	public GameObject objX;
	public GameObject objY;
	public GameObject objResult;
	public float numX;
	public float numY;
	public float result;

	// Use this for initialization
	void Start () {
		SetPersonalDetails("Ethan", 19, "Male");
		objX.GetComponent<Text>().text = "0";
		objY.GetComponent<Text>().text = "0";
	}

	// Update is called once per frame
	void Update (){
		numX = float.Parse(objX.GetComponent<Text>().text);
		numY = float.Parse(objY.GetComponent<Text>().text);
		objResult.GetComponent<Text>().text = result.ToString();
	}

	#region SetFunctions
	private void SetName(string name){
		myName = name;
		print(name);
	}

	private void SetAge(int age){
		myAge = age;
		print (age);
	}

	private void SetGender(string gender){
		myGender = gender;
		print(gender);
	}

	private void SetPersonalDetails(string name, int age, string gender){
		SetName(name);
		SetAge(age);
		SetGender(gender);
	}
	#endregion

	#region Calculator
	//subtract
	public void ExecuteSubtract(){
		SubtractNumbers(numX, numY);
	}
	public void SubtractNumbers(float numA, float numB){
		result = numA - numB;
	}

	//Add
	public void ExecuteAdd(){
		AddNumbers(numX, numY);
	}
	public void AddNumbers(float numA, float numB){
		result = numA + numB;
	}

	//multiply
	public void ExucuteMultply(){
		MultiplyNumbers(numX, numY);
	}
	public void MultiplyNumbers(float numA, float numB){
			result = numA * numB;
	}

	//Devide
	public void ExecuteDivision(){
		DivideNumbers(numX, numY);
	}
	public void DivideNumbers(float numA, float numB){
			result = numA / numB;
	}

	//To the power
	public void ExecutePower(){
		PowerNumbers(numX, numY);
	}
	public void PowerNumbers(float numA, float numB){
		result = Mathf.Pow(numA, numB);
	}

	#endregion
	
}
