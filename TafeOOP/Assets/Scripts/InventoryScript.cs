﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PlayerInventory{
	public string name;
	public string description;
	public string useMessage;
}

public class InventoryScript : MonoBehaviour {

	public int currentIndex;
	public PlayerInventory[] items;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		CheckKeys();
	}

	private void CheckKeys(){
		if(Input.GetKeyDown(KeyCode.RightBracket)){
			IncreaseIndex();
		}
		
		if(Input.GetKeyDown(KeyCode.LeftBracket)){
			DecreaseIndex();
		}

		if(Input.GetKeyDown(KeyCode.Space)){
			UseItem();
		}
	}

	public void UseItem(){

		print(items[currentIndex].useMessage);
	}

	private void DecreaseIndex(){
		currentIndex --;
		if(currentIndex < 0){
			currentIndex = 0;
		}
	}
	
	private void IncreaseIndex(){
		currentIndex ++;
		if(currentIndex > items.Length - 1){
			currentIndex = items.Length - 1;
		}
	}
}
