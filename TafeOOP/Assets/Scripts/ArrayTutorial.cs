﻿using UnityEngine;
using System.Collections;

public class ArrayTutorial : MonoBehaviour {

	public uint arraySize;
	public string[] studentNames;

	// Use this for initialization
	void Start () {
		studentNames = new string[arraySize];
		setNames();
		PrintNames();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private void setNames(){
//		studentNames[0] = "Gypsy One";
//		studentNames[1] = "Gypsy Two";
//		studentNames[2] = "Gypsy Three";
//		studentNames[3] = "Gypsy Four";
//		studentNames[4] = "Gypsy Five";
//		studentNames[5] = "Gypsy Six";
		for(int i = 0; i < studentNames.Length; i++){
			studentNames[i] = "Student No." + (i + 1);
		}
	}

	private void PrintNames(){
		for(int i = 0; i < studentNames.Length; i++){
			print(studentNames[i]);
		}
		/*
		print ("Name One: " + studentNames[0]);
		print ("Name Two: " + studentNames[1]);
		print ("Name Three: " + studentNames[2]);
		print ("Name Four: " + studentNames[3]);
		print ("Name Five: " + studentNames[4]);
		print ("Name Six: " + studentNames[5]);
		*/
	}
}
