﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour {

	public string myName;
	public int myAge;
	public bool isMale;
	private string randomText;
	public float playerSpeed;

// Use this for initialization
	void Start () {
		myName = "Ethan";
		myAge = 19;
		isMale = true;
		playerSpeed = 0.3f;
		randomText = "a random sentence.";
//		PrintID();
	}
	
// Update is called once per frame
	void Update () {
		CheckKeyPresses();
	}

	public void PrintMyName(){
//		print (myName);
	}
	private void PrintID(){
		print (myName);
		print (myAge);
		if(isMale == true){
			print("male");
		}
		print (randomText);
	}

	private void CheckKeyPresses(){
		if(Input.GetKey(KeyCode.W)){
			MovePlayerForward();
			print ("W");
		}
		if(Input.GetKey(KeyCode.A)){
			MovePlayerLeft();
			print ("A");
		}
		if(Input.GetKey(KeyCode.S)){
			MovePlayerBackward();
			print ("S");
		}
		if(Input.GetKey(KeyCode.D)){
			MovePlayerRight();
			print ("D");
		}
	}

	private void MovePlayerForward(){
		if(Input.GetKey(KeyCode.W)){
			transform.Translate(Vector3.forward * playerSpeed);
		}
	}

	private void MovePlayerRight(){
		if(Input.GetKey(KeyCode.D)){
			transform.Translate(Vector3.right * playerSpeed);
		}
	}

	private void MovePlayerBackward(){
		if(Input.GetKey(KeyCode.S)){
			transform.Translate(Vector3.back * playerSpeed);
		}
	}

	private void MovePlayerLeft(){
		if(Input.GetKey(KeyCode.A)){
			transform.Translate(Vector3.left * playerSpeed);
		}
	}

}
