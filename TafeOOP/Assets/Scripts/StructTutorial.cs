﻿using UnityEngine;
using System.Collections;

[System.Serializable] 	//This makes it visable in the inspector
public struct Adress{ 	//This is declaring the struct
	public int streetNumber;
	public string streetName;
	public int postcode;
	public string suburbName;
	public string stateName;
	public string countryName;
}


public class StructTutorial : MonoBehaviour {

	public Adress myAdress;	//making a variable using struct name as the type
	public KeyBinds myKeyBinds;

	// Use this for initialization
	void Start () {

		SetStartingAdress();
//		print ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			ChangeAdress();
		}
	}

	private void KeyBindings(){
		myKeyBinds.keyForward = KeyCode.A;
	}

	private void SetStartingAdress(){
		myAdress.streetNumber = 213;
		myAdress.streetName = "Pacific Hwy";
		myAdress.postcode = 2065;
		myAdress.suburbName = "St. Leonards";
		myAdress.stateName = "NSW";
		myAdress.countryName = "Australia";
	}

	private void ChangeAdress(){
		myAdress.streetNumber = 123;
		myAdress.streetName = "Fake St.";
		myAdress.postcode = 1000;
		myAdress.suburbName = "Springfeild";
		myAdress.stateName = "???";
		myAdress.countryName = "'Merica";
	}

}


