﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct GeneralStats{
	public string playerName;
	public float moveSpeed;
	public float jumpSpeed;
//	public float curHealth;
	public float maxHealth;
	public KeyBinds myKeybinds;
}

public class Player : MonoBehaviour {

	public GeneralStats myStats;
	private Rigidbody myRigidBody;
	private GameObject thisPlayer;

	// Use this for initialization
	void Start () {
		myRigidBody = GetComponent<Rigidbody>();
		thisPlayer = GetComponent<GameObject>();
		MyStatistics();
	}
	
	// Update is called once per frame
	void Update () {
		PlayerMovement();
		if(myStats.maxHealth <= 0){
			Destroy(thisPlayer);
			print ("player has died");
		}
	}

	private void PlayerMovement(){
		
		if(Input.GetKeyDown(myStats.myKeybinds.keyJump)){
			myRigidBody.AddForce(Vector3.up * myStats.jumpSpeed , ForceMode.Impulse);
		}
		if(Input.GetKey(myStats.myKeybinds.keyForward)){
			myRigidBody.AddForce(Vector3.forward * myStats.moveSpeed);
		}
		if(Input.GetKey(myStats.myKeybinds.keyLeft)){
			myRigidBody.AddForce(Vector3.left * myStats.moveSpeed);
		}
		if(Input.GetKey(myStats.myKeybinds.keyBack)){
			myRigidBody.AddForce(Vector3.back * myStats.moveSpeed);
		}
		if(Input.GetKey(myStats.myKeybinds.keyRight)){
			myRigidBody.AddForce(Vector3.right * myStats.moveSpeed);
		}
	}

	public void MyStatistics(){
		myStats.playerName = "Name";
		myStats.moveSpeed = 15;
		myStats.jumpSpeed = 10;
//		myStats.curHealth = 100;
		myStats.maxHealth = 100;
		myStats.myKeybinds.keyForward = KeyCode.W;
		myStats.myKeybinds.keyLeft = KeyCode.A;
		myStats.myKeybinds.keyBack = KeyCode.S;
		myStats.myKeybinds.keyRight = KeyCode.D;
		myStats.myKeybinds.keyJump = KeyCode.Space;
	}

	void OnCollisionEnter(Collision other){
		if(other.gameObject.tag == "Spikes"){
			myStats.maxHealth -= 10;
		}
	}
}
