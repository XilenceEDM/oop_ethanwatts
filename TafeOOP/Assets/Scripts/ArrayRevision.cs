﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour {

	public int arraySize;
	public int currentIndex;
	public int[] collectiveAges;
		
	// Use this for initialization
	void Start () {
		arraySize = 5;
		collectiveAges = new int[arraySize];
	//	LoopTask();
	}
	
	// Update is called once per frame
	void Update () {
		CheckKeys();
	}

	private void LoopTask(){
		for(int i = 0; i < arraySize; i++){
			collectiveAges[i] = -1;
			print (collectiveAges[i]);
		}
	}
	
	private void CheckKeys(){
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			IncreaseIndex();
		}

		if(Input.GetKeyDown(KeyCode.DownArrow)){
			DecreaseIndex();
		}

		if(Input.GetKeyDown(KeyCode.Return)){
			SetAge();
		}
	}
	
	public void SetAge(){
		collectiveAges[currentIndex] = 20;
	}

	private void DecreaseIndex(){
		currentIndex --;
		if(currentIndex < 0){
			currentIndex = 0;
		}
	}

	private void IncreaseIndex(){
		currentIndex ++;
		if(currentIndex > arraySize - 1){
			currentIndex = arraySize - 1;
		}
	}

}