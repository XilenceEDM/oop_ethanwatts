﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

	private Rigidbody myRigidbody;
	public float moveSpeed;

	// Use this for initialization
	void Start () {
		myRigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		CheckKeys();
	}
	/* Moving directions
//	private void MoveForward(){
//		myRigidbody.AddForce(Vector3.forward * moveSpeed);
//	}
//
//	private void MoveBack(){
//		myRigidbody.AddForce(Vector3.back * moveSpeed);
//	}
//
//	private void MoveLeft(){
//		myRigidbody.AddForce(Vector3.left * moveSpeed);
//	}
//
//	private void MoveRight(){
//		myRigidbody.AddForce(Vector3.right * moveSpeed);
//	}
*/

	private void Move(Vector3 direction){
		myRigidbody.AddForce(direction * moveSpeed);
	}

	private void CheckKeys(){
		if(Input.GetKey(KeyCode.W)){
			Move(Vector3.forward);
		}
		if(Input.GetKey(KeyCode.A)){
			Move(Vector3.left);
		}
		if(Input.GetKey(KeyCode.S)){
			Move(Vector3.back);
		}
		if(Input.GetKey(KeyCode.D)){
			Move(Vector3.right);
		}
	}
}
