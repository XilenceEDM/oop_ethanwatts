﻿using UnityEngine;
using System.Collections;

public class Raycasting : MonoBehaviour {

	//Script for the raycasting tutorial
/*
	Physics.Raycast(Vector3 'origin',
	                Vector3 'direction of ray',
	                RaycastHit 'stores info of colliders the ray has hit',
	                float 'the distance of the ray', //will default to infinite if not specified 
	                LayerMask 'a number of a particular layer which you can place objects that the ray will ignore'// not needed
	                );
*/

	// Use this for initialization
	void Start () {

		// For example:
		if(Physics.Raycast(transform.position, Vector3.down, 1)){
			//this will detect anything 1 unit under the object.
		}

	}



	// Update is called once per frame
	void Update () {
		
	}


}
